package com.rambris.jacksondate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JacksondateApplication {

    public static void main(String[] args) {
        SpringApplication.run(JacksondateApplication.class, args);
    }

}
