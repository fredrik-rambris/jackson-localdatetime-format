package com.rambris.jacksondate;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.ZoneId;

@RestController
@RequestMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
public class TestController {

    @PostMapping("test")
    public void test(@RequestBody TestModel model) {
        log.info("{}", model.getDatumen().withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime());
    }
}
