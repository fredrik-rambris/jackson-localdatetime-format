package com.rambris.jacksondate;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class TestModel {
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSSSSS", timezone="UTC")
    private ZonedDateTime datumen;
}
